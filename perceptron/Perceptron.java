/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package perceptron;

/**
 *
 * @author Philippe
 */
public class Perceptron {
    
    double alpha;
    int[][] target;
    double[][] input;
    
    inNeuron[] inNeurons;
    outNeuron[] outNeurons;
    link[] links;
    bias[] biases;
    
    int trainingPairIndex;
    
    public Perceptron(int[][] target, double[][] input, double alpha, inNeuron[] inNeurons, outNeuron[] outNeurons, bias[] biases, link[] links){
        this.target = target;
        this.input = input;
        this.alpha = alpha;
        this.inNeurons = inNeurons;
        this.outNeurons = outNeurons;
        this.links = links;
        this.alpha = alpha;
        this.biases = biases;
    }
    
    public void run(){
        boolean weightChanged;
        for (int i = 0; i < input.length; i++) {
            trainingPairIndex = i;
            weightChanged = true;
            while (weightChanged) {
                resetOutNeurons();
                weightChanged = false;
                sendInSignals();
                computeResponseForOutputUnits();
                if (checkAnswers() == false) {
                    weightChanged = true;
                    setWeights();
                }
            }
        }
    }
    
    public void resetOutNeurons(){
        for(int i=0; i<outNeurons.length; i++){
            outNeurons[i].zeroInput();
        }
    }
    
    public void sendInSignals(){
        for(int i=0; i<inNeurons.length; i++){
            inNeurons[i].receiveSignal(input[trainingPairIndex][i]);
        }
    }
    
    public void computeResponseForOutputUnits(){
        for(int i=0; i<links.length; i++){
            links[i].sendSignal();
        }
        for(int i=0; i<biases.length; i++){
            biases[i].sendSignal();
        }
    }
    
    public boolean checkAnswers(){
        for(int i=0; i<outNeurons.length; i++){
            if(outNeurons[i].sendSignal()!=target[trainingPairIndex][i]){
                return false;
            }
        }
        return true;
    }
    
    public void setWeights(){
        double newWeight;
        int count = 0;
        for(int i=0; i<inNeurons.length; i++){
            for(int j=0; j<outNeurons.length; j++){
                newWeight = links[count].getWeight() + (target[trainingPairIndex][j]*input[trainingPairIndex][i]*alpha);
                links[count].setWeight(newWeight);
                count++;
            }
        }

        double newBias;
        for(int i=0; i<biases.length; i++){
            newBias = biases[i].getWeight() + (alpha*target[trainingPairIndex][i]);
            biases[i].setWeight(newBias);
        }
    }
    
    public void printNeurons(){
        System.out.println("Neuronios de entrada:");
        for(int i=0; i<inNeurons.length; i++){
            System.out.println(i + " Entrada: " + inNeurons[i].sendSignal());
        }
        System.out.println("Neuronios de saida:");
        for(int i=0; i<outNeurons.length; i++){
            System.out.println(i + " Resposta: " + outNeurons[i].sendSignal());
        }
    }
    
}
