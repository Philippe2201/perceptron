/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package perceptron;

/**
 *
 * @author Philippe
 */
public class bias {
    
    outNeuron output;
    double weight;
    
    public bias(outNeuron output, double weight){
        this.output = output;
        this.weight = weight;
    }
    
    public void sendSignal(){
        output.reciveSignal(weight);
    }
    
    public void setWeight(double weight){
        this.weight = weight;
    }
    
    public double getWeight(){
        return weight;
    }
    
}
