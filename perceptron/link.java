/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package perceptron;

/**
 *
 * @author Philippe
 */
public class link {
    
    double weight;
    public inNeuron input;
    public outNeuron output;
    
    link(inNeuron input, outNeuron output){
        this.input = input;
        this.output = output;
        weight = 0;
    }
    
    public void sendSignal(){
        output.reciveSignal(input.sendSignal()*weight);
    }
    
    public void setWeight(double weight){
        this.weight = weight;
    }
    
    public double getWeight(){
        return weight;
    }
        
}
