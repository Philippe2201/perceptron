package perceptron;

/**
 * Class that represents an input neuron
 *
 * @author Philippe
 */
public class inNeuron {
    
    protected double inSignal;
    
    public void receiveSignal(double inSignal){
        this.inSignal = inSignal;
    }
    
    public double sendSignal(){
        return inSignal;
    }
    
}
