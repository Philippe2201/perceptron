/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package perceptron;

/**
 *
 * @author Philippe
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    static final int dimensions = 2;
    static final int inputSize = 4;
    static final int targetSize = 1;
    static final double theta = 0.5;
    static final double alpha = 0.1;
    static readInputFile reader;
    
    public static void main(String[] args) {
        //caminho do arquivo de entrada dos dados
        reader = new readInputFile("/Volumes/Files HD/Dropbox/Perceptron/Perceptron/or.txt", inputSize, dimensions, targetSize);
        Perceptron();
    }
    
    public static void Perceptron(){
        inNeuron[] inNeurons = initializeInNeurons();
        outNeuron[] outNeurons = initializeOutNeurons();
        bias[] biases = initializeBiases(outNeurons);
        link[] links = initializeLinks(inNeurons, outNeurons);
        
        double[][] input = reader.read();
        int[][] target = reader.getTarget();
        
        Perceptron perceptron = new Perceptron(target, input, alpha, inNeurons, outNeurons, biases, links);
        perceptron.run();
        System.out.println("Resposta: ");
        for(int i=0; i<links.length; i++){
            System.out.println(links[i].getWeight());
        }
        test(inNeurons, outNeurons, biases, links);
    }
    
    public static inNeuron[] initializeInNeurons(){
        inNeuron[] inNeurons = new inNeuron[dimensions];
        for(int i=0; i<inNeurons.length; i++){
            inNeurons[i] = new inNeuron();
        }
        return inNeurons;
    }
    
    public static outNeuron[] initializeOutNeurons(){
        outNeuron[] outNeurons = new outNeuron[targetSize];
        activationFuntion activation = new activationFuntion(theta);
        for(int i=0; i<outNeurons.length; i++){
            outNeurons[i] = new outNeuron(activation);
        }
        return outNeurons;
    }
    
    public static link[] initializeLinks(inNeuron[] inNeurons, outNeuron[] outNeurons){
        link[] conections = new link[inNeurons.length*outNeurons.length];
        int count = 0;
        for(int i=0; i<inNeurons.length; i++){
            for(int j=0; j<outNeurons.length; j++){
                conections[count] = new link(inNeurons[i], outNeurons[j]);
                count++;
            }
        }
        return conections;
    }
    
    public static bias[] initializeBiases(outNeuron[] outNeurons){
        bias[] biases = new bias[outNeurons.length];
        for(int i=0; i<outNeurons.length; i++){
            biases[i] = new bias(outNeurons[i], 0);
        }
        return biases;
    }
    
    public static void test(inNeuron inNeurons[], outNeuron[] outNeurons, bias[] biases, link[] links){
        readInputFile read = new readInputFile("/Volumes/Files HD/Dropbox/Perceptron/Perceptron/or.txt", 4, dimensions, targetSize);
        double[][] test = read.read();

        int[][] target = read.getTarget();
        
        int acertos = 0;
        
        for(int i=0; i<test.length; i++){
            for(int j=0; j<outNeurons.length; j++){
                outNeurons[j].zeroInput();
            }
            System.out.print("Testando para o par: ");
            for(int j=0; j<inNeurons.length; j++){
                System.out.print(test[i][j] + " ");
                inNeurons[j].receiveSignal(test[i][j]);
            }
            System.out.println();
            for(int j=0; j<links.length; j++){
                links[j].sendSignal();
            }
            for(int j=0; j<biases.length; j++){
                biases[j].sendSignal();
            }
            System.out.print("Total dos neuronios de saída: ");
            boolean error = false;
            for(int j=0; j<outNeurons.length; j++){
                System.out.println(outNeurons[j].getInput());
                if(outNeurons[j].sendSignal() != target[i][j]){
                    error = true;
                }
            }
            if(error){
                System.out.println("Resposta Errada!");
            }else{
                acertos++;
                System.out.println("Resposta Correta!");
            }
        }
        System.out.println("Total de acertos: " + acertos);
        double porcentagemDeAcertos = (100*acertos)/target.length;
        System.out.println("Porcentagem de acertos: " + porcentagemDeAcertos);
    }
}
