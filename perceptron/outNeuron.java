package perceptron;

/**
 * Class that represents an output neuron
 * 
 * @author Philippe
 */
public class outNeuron {
    
    protected double input;
    protected int output;
    activationFuntion activation;
    
    public outNeuron(activationFuntion activation){
        this.activation = activation;
    }
    
    public void reciveSignal(double input){
        this.input += input;
        //System.out.println("Sinal recebido: " + input + " somaParcial: " + this.input);
    }
    
    public int sendSignal(){
        return activation.funtion(input);
    }
    
    public void zeroInput(){
        input = 0;
    }
    
    public double getInput(){
        return input;
    }
    
}
