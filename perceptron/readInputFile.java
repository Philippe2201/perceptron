/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package perceptron;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Philippe
 */
public class readInputFile {

    File file;
    int inputSize;
    int dimensions;
    int targetSize;

    public readInputFile(String path, int inputSize, int dimensions, int targetSize) {
        file = new File(path);
        this.inputSize = inputSize;
        this.dimensions = dimensions;
        this.targetSize = targetSize;
    }

    public double[][] read() {
        double[][] input = new double[inputSize][dimensions];

        try {
            Scanner scan = new Scanner(file);
            String[] array;
            int i = 0;
            while (scan.hasNext()) {
                array = scan.nextLine().split(",");
                for (int j = 0; j < array.length - 1; j++) {
                    input[i][j] = Double.parseDouble(array[j]);
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.toString());
        }

        return input;
    }

    public int[][] getTarget() {
        int[][] target = new int[inputSize][targetSize];

        try {
            Scanner scan = new Scanner(file);
            String[] array;
            int i = 0;
            while (scan.hasNext()) {
                array = scan.nextLine().split(",");
                for (int k = 0; k < array[0].length(); k++) {
                    if((int) (array[array.length - 1].charAt(k) - 48) == 0){
                        target[i][k] = 0;
                    }else{
                        target[i][k] = (int) (array[array.length - 1].charAt(k) - 48);
                    }
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.toString());
        }

        return target;
    }
}